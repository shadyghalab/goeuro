//
//  SearchResultController.h
//  GoEuro
//
//  Created by Shady Ghalab on 9/27/15.
//  Copyright © 2015 Shady Ghalab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TravelSearchModel.h"


typedef void(^selectedResult)(TravelSearchModel *);

@interface SearchResultController : UIViewController
@property(nonatomic,weak)IBOutlet UITableView *tableview;
@property (nonatomic,strong) NSMutableArray *searchResultList;
@property(readwrite, copy) selectedResult selectedItem;


@end
