//
//  NetworkOperations.m
//  GoEuro
//
//  Created by Shady Ghalab on 9/27/15.
//  Copyright © 2015 Shady Ghalab. All rights reserved.
//

#import "NetworkOperations.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>


@implementation NetworkOperations

+(void)searchForTravelAtGoEuro:(NSString *)term andLocale:(NSString *)locale response:(void (^)(NSMutableArray *, NSError *))completion{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"http://api.goeuro.com/api/v2/position/suggest/%@/%@",locale,term];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (responseObject != nil) {
            
            NSMutableArray *results = [[NSMutableArray alloc] init];
            NSError * error;
            for (NSDictionary *obj in responseObject) {
                TravelSearchModel *model = [[TravelSearchModel alloc] initWithDictionary:obj error:&error];
                [results addObject:model];
            }
            
            
            if (results.count > 0) {
                if (completion) {
                    completion(results,error);
                }
            }else{
                if (completion) {
                    completion(nil,error);
                }
                
            }
        }else{
            if (completion) {
                completion(nil,operation.error);
            }
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completion) {
            completion(nil,operation.error);
        }
    }];

}


+(void)cancelAllRequest{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.operationQueue cancelAllOperations];
}


@end
