//
//  TravelSearchModel.h
//  GoEuro
//
//  Created by Shady Ghalab on 9/27/15.
//  Copyright © 2015 Shady Ghalab. All rights reserved.
//

#import <JSONModel/JSONModel.h>


@protocol GEOposition

@end

@interface GEOposition : JSONModel

@property(assign,nonatomic) double longitude;
@property(assign,nonatomic) double latitude;

@end


@interface TravelSearchModel : JSONModel

@property(assign,nonatomic) double _id;
@property(strong,nonatomic) NSString <Optional> *key;
@property(strong,nonatomic) NSString  *name;
@property(strong,nonatomic) NSString  *fullName;
@property(strong,nonatomic) NSString <Optional> *iata_airport_code;
@property(strong,nonatomic) NSString  *type;
@property(strong,nonatomic) NSString  *country;
@property(assign,nonatomic) NSNumber <Optional> *locationId;
@property(assign,nonatomic) BOOL inEurope;
@property(strong,nonatomic) NSString  *countryCode;
@property(assign,nonatomic) BOOL coreCountry;
@property(strong,nonatomic) NSNumber <Optional>  *distance;
@property (nonatomic,strong)GEOposition *geo_position;
@property(strong,nonatomic) NSNumber <Optional>  *NearestDistance;

@end

