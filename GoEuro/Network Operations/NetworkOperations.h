//
//  NetworkOperations.h
//  GoEuro
//
//  Created by Shady Ghalab on 9/27/15.
//  Copyright © 2015 Shady Ghalab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TravelSearchModel.h"

@interface NetworkOperations : NSObject
+(void)searchForTravelAtGoEuro:(NSString *)term andLocale:(NSString *)locale response:(void (^)(NSMutableArray *, NSError *))completion;
+(void)cancelAllRequest;
@end
