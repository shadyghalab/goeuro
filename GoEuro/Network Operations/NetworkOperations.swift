//
//  NetworkOperations.swift
//  GoEuro
//
//  Created by Shady Ghalab on 9/27/15.
//  Copyright © 2015 Shady Ghalab. All rights reserved.
//
import Alamofire


@objc class NetworkOperations : NSObject
{
    
    
    class func searchForTravelAtGoEuro(term:String,locale:String,completion:(destinations:[TravelSearchModel]?,error:NSError?)->Void)->(){
        
    Alamofire.Manager.sharedInstance.request(.GET, "http://api.goeuro.com/api/v2/position/suggest/\(locale)/\(term)").responseJSON { (resquest, response, result, error) -> Void in
       
        if error == nil{
        
            let results = result as? [NSDictionary]
        
            var destinations = [TravelSearchModel]()
            for result in results!{
            
                do
                {
                    let searchModel = try TravelSearchModel(dictionary: result as [NSObject : AnyObject])
                    destinations.append(searchModel)
                }
                catch{
                    let nserror = error as! NSError
                    NSLog("error \(nserror), \(nserror.userInfo)")
                    completion(destinations: nil,error: nserror)
                }
            }
           
            completion(destinations: destinations,error: error)

        }
        else{
            completion(destinations: nil,error: error)
        }
        
    
    }

}


    
    class func cancelAllRequest(){
        Alamofire.Manager().session.invalidateAndCancel()
    }
    
    
}

