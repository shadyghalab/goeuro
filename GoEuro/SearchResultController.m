//
//  SearchResultController.m
//  GoEuro
//
//  Created by Shady Ghalab on 9/27/15.
//  Copyright © 2015 Shady Ghalab. All rights reserved.
//

#import "SearchResultController.h"
#import "SearchResultCell.h"
#import "TravelSearchModel.h"
#include <math.h>

@interface SearchResultController ()
@end


@implementation SearchResultController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addObserver:self forKeyPath:@"searchResultList" options:NSKeyValueObservingOptionNew context:nil];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.searchResultList.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SearchResultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchResultCell" forIndexPath:indexPath];
    
    TravelSearchModel *travelModel = [self.searchResultList objectAtIndex:indexPath.row];
    cell.name.text = travelModel.fullName;
    cell.nearestDistance.text = convertDistanceToString(travelModel.NearestDistance.floatValue);

    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    TravelSearchModel *travelModel = [self.searchResultList objectAtIndex:indexPath.row];
    if (self.selectedItem) {
        self.selectedItem(travelModel);
    }


}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    if([keyPath isEqualToString:@"searchResultList"])
    {
        [self.tableview reloadData];
    }
}

NSString* convertDistanceToString(float distance) {
    if (distance < 100)
        return [NSString stringWithFormat:@"%g m", roundf(distance)];
    else if (distance < 1000)
        return [NSString stringWithFormat:@"%g m", roundf(distance/5)*5];
    else if (distance < 10000)
        return [NSString stringWithFormat:@"%g km", roundf(distance/100)/10];
    else
        return [NSString stringWithFormat:@"%g km", roundf(distance/1000)];
}

@end
