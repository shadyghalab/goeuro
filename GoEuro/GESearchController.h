//
//  ViewController.h
//  GoEuro
//
//  Created by Shady Ghalab on 9/27/15.
//  Copyright © 2015 Shady Ghalab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface GESearchController : UIViewController
@property (nonatomic,retain) CLLocationManager *locationManager;

@end

