//
//  AppDelegate.h
//  GoEuro
//
//  Created by Shady Ghalab on 9/27/15.
//  Copyright © 2015 Shady Ghalab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

