//
//  ViewController.m
//  GoEuro
//
//  Created by Shady Ghalab on 9/27/15.
//  Copyright © 2015 Shady Ghalab. All rights reserved.
//

#import "GESearchController.h"
#import "Network Operations/NetworkOperations.h"
#import "SearchResultController.h"
#import <THCalendarDatePicker/THDatePickerViewController.h>

@interface GESearchController ()<THDatePickerDelegate>
@property (nonatomic,strong) NSMutableArray *searchResultList;
@property (nonatomic,strong) SearchResultController *searchResultController;
@property (nonatomic,strong) THDatePickerViewController *datePicker;

@property (nonatomic,weak) IBOutlet UIButton *searchBTN;
@property (nonatomic,weak) IBOutlet UILabel *dateLabel;

@property (nonatomic,weak) IBOutlet UITextField *fromField;
@property (nonatomic,weak) IBOutlet UITextField *toField;


@end

@implementation GESearchController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"GoEuro";
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.searchResultController = [storyboard instantiateViewControllerWithIdentifier:@"SearchResultID"];
    self.searchBTN.layer.cornerRadius = 5;
    [self.searchBTN setEnabled:NO];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:1/255.0 green:87/255.0 blue:146/255.0 alpha:1]}];
   
     self.dateLabel.text = [GESearchController getDate:[NSDate date]];
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager requestWhenInUseAuthorization];
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [self.locationManager startUpdatingLocation];
}


- (IBAction)pickDateFromCalendar:(id)sender {
    if(!self.datePicker)
    self.datePicker = [THDatePickerViewController datePicker];
    self.datePicker.date = [NSDate date];
    self.datePicker.delegate = self;
    [self.datePicker setAllowClearDate:NO];
    [self.datePicker setClearAsToday:YES];
    [self.datePicker setAutoCloseOnSelectDate:YES];
    [self.datePicker setAllowSelectionOfSelectedDate:YES];
    [self.datePicker setDisableHistorySelection:YES];
    [self.datePicker setDisableFutureSelection:NO];
    [self.datePicker setSelectedBackgroundColor:[UIColor colorWithRed:125/255.0 green:208/255.0 blue:0/255.0 alpha:1.0]];
    [self.datePicker setCurrentDateColor:[UIColor colorWithRed:242/255.0 green:121/255.0 blue:53/255.0 alpha:1.0]];
    
    [self.datePicker setDateHasItemsCallback:^BOOL(NSDate *date) {
        int tmp = (arc4random() % 30)+1;
        if(tmp % 5 == 0)
            return YES;
        return NO;
    }];
    //[self.datePicker slideUpInView:self.view withModalColor:[UIColor lightGrayColor]];
    [self presentSemiViewController:self.datePicker withOptions:@{
                                                                  KNSemiModalOptionKeys.pushParentBack    : @(NO),
                                                                  KNSemiModalOptionKeys.animationDuration : @(1.0),
                                                                  KNSemiModalOptionKeys.shadowOpacity     : @(0.3),
                                                                  }];
}


-(void)datePickerDonePressed:(THDatePickerViewController *)datePicker{
    
    [self.datePicker dismissSemiModalView];
}
-(void)datePickerCancelPressed:(THDatePickerViewController *)datePicker{
    [self.datePicker dismissSemiModalView];

}


-(void)datePicker:(THDatePickerViewController *)datePicker selectedDate:(NSDate *)selectedDate{

    self.dateLabel.text = [GESearchController getDate:selectedDate];

}
-(void)datePickerDidHide:(THDatePickerViewController *)datePicker{

}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    
    if ([self.fromField.text length] >0 && [self.toField.text length]) {
        [self.searchBTN setEnabled:YES];
    }else
        [self.searchBTN setEnabled:NO];


    if (![string isEqualToString:@"\n"]) {
  
     NSString *termText =  [textField.text stringByReplacingCharactersInRange:range withString:string];

     if (![termText isEqualToString:@""]&& termText.length >= 2) {
    
        [NetworkOperations cancelAllRequest];
       
        if (![textField viewWithTag:5000]) {
         
            UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            spinner.tag = 5000;
            [spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
            [textField addSubview:spinner];
            
            [spinner addConstraint:[NSLayoutConstraint constraintWithItem:spinner attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:30]];
            [spinner addConstraint:[NSLayoutConstraint constraintWithItem:spinner attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:30]];
            
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:textField attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:spinner attribute:NSLayoutAttributeRight multiplier:1 constant:3]];
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:textField attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:spinner attribute:NSLayoutAttributeTop multiplier:1 constant:-1]];
            [spinner startAnimating];
        }
        
        
        __weak GESearchController *weakSelf = self;
        [NetworkOperations searchForTravelAtGoEuro:termText andLocale:@"de" response:^(NSMutableArray *results, NSError *error) {
            
            if(error == nil && results.count > 0){
              
               
              
                if (![weakSelf.view viewWithTag:1000]) {
                    weakSelf.searchResultController.view.translatesAutoresizingMaskIntoConstraints = NO;
                    weakSelf.searchResultController.view.tag = 1000;
                    weakSelf.searchResultController.searchResultList = [self orderByDistance:results];
                    [weakSelf.view addSubview:weakSelf.searchResultController.view];
                    
                    weakSelf.searchResultController.selectedItem = ^(TravelSearchModel *selectedItem){
                        textField.text = selectedItem.name;
                        [[weakSelf.view viewWithTag:1000] removeFromSuperview];
                        [weakSelf.view  endEditing:YES];
                    };
                    
                    
                    [weakSelf.view addConstraint:[NSLayoutConstraint constraintWithItem:textField attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationLessThanOrEqual toItem:weakSelf.searchResultController.view attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
                    [weakSelf.view addConstraint:[NSLayoutConstraint constraintWithItem:textField attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:weakSelf.searchResultController.view attribute:NSLayoutAttributeTrailing multiplier:1 constant:0]];
                    [weakSelf.view addConstraint:[NSLayoutConstraint constraintWithItem:textField attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:weakSelf.searchResultController.view attribute:NSLayoutAttributeLeading multiplier:1 constant:0]];
                    [weakSelf.searchResultController.view addConstraint:[NSLayoutConstraint constraintWithItem:weakSelf.searchResultController.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:200]];
                    [weakSelf.view addConstraint:[NSLayoutConstraint constraintWithItem:weakSelf.searchResultController.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:textField attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
                }else{
                    weakSelf.searchResultController.searchResultList = [self orderByDistance:results];
                }
              
            
                [[textField viewWithTag:5000]removeFromSuperview];
                
            }
            
            
            
        }];
    
        
    }
    
    }
    
    return YES;

}


-(NSMutableArray *)orderByDistance:(NSMutableArray *)results{

    if (self.locationManager.location){
    
        for (TravelSearchModel *result in results) {
            CLLocation *currentLoc = self.locationManager.location;
            CLLocation *destination = [[CLLocation alloc] initWithLatitude:result.geo_position.latitude longitude:result.geo_position.longitude];
            double meters = [destination distanceFromLocation:currentLoc];
            result.NearestDistance = [NSNumber numberWithDouble:meters];
        }
        
        NSSortDescriptor* sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"NearestDistance" ascending:YES];
        [results sortUsingDescriptors:[NSArray arrayWithObject:sortByDate]];
        
    }

    return results;
    

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [[self.view viewWithTag:1000] removeFromSuperview];
    return YES;
}

-(IBAction)searchForTravelling:(id)sender{

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning!" message:@"Search is not yet implemented" preferredStyle:UIAlertControllerStyleAlert];
 
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Ok"
                                           style:UIAlertActionStyleDefault
                                         handler:nil];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

+(NSString *)getDate:(NSDate *)date {
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    formatter.timeZone = destinationTimeZone;
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    return [formatter stringFromDate:date];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
