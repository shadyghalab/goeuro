//
//  SearchResultCell.h
//  GoEuro
//
//  Created by Shady Ghalab on 9/27/15.
//  Copyright © 2015 Shady Ghalab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel *name;
@property(nonatomic,weak)IBOutlet UILabel *nearestDistance;

@end
